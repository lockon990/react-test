import React from 'react';

const pilot = (props) => {
    return (
        <div>
            <p onClick={props.click}>{props.ms} piloted by {props.name}</p>
            <p>{props.children}</p>
        </div>
    )
};

export default pilot;