import React, { useState } from 'react';
import './App.css';
import Pilot from './Pilot/Pilot';

const App = props => {

    const [ pilotsState, setPilotsState ] = useState({
      pilots: [
        { name: "Kira Yamato", ms: "Freedom Gundam" },
        { name: "Athrun Zala", ms: "Justice Gundam" },
        { name: "Shinn Asuka", ms: "Force Impulse Gundam"}
      ],
    });

    const [otherState, setOtherState] = useState('some other shit')

    console.log(pilotsState, otherState);

    const switchMSHandler = (newMS) => {
      setPilotsState(  
        {
          pilots: [
            { name: "Kira Yamato", ms: "Strike Freedom Gundam" },
            { name: "Athrun Zala", ms: "Infinite Justice Gundam" },
            { name: "Shinn Asuka", ms: newMS}
          ],
          otherState: pilotsState.otherState
        }
      )
    };

    return (
      <div className="App">
        <h1>Seed Ace Pilots</h1>
        <button onClick={ () => switchMSHandler('Gaia Impulse Gundam') }>Upgrade MS</button> {/*inefficient*/}

        <Pilot 
          name={pilotsState.pilots[0].name} 
          ms={pilotsState.pilots[0].ms}
        />

        <Pilot 
          name={pilotsState.pilots[1].name} 
          ms={pilotsState.pilots[1].ms}
        />

        <Pilot 
          name={pilotsState.pilots[2].name} 
          ms={pilotsState.pilots[2].ms}
          click = {switchMSHandler.bind(this,'Destiny Impulse Gundam')}>
          Achievement: FAITH Member
        </Pilot>
      </div>
    );
  }

export default App;
